﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {
        //Variaveis usada para o dsenvolvimento das fases
        int informacao_ganhador1 = 0;
        int informacao_ganhador2 = 0;
        int informacao_ganhador3 = 0;
        int informacao_ganhador4 = 0;

        int informacao_ganhador_rodada2_1 = 0;
        int informacao_ganhador_rodada2_2 = 0;

        string ganhador_rodada1_jogo1;
        double nota_ganhador_rodada1;

        string ganhador_rodada1_jogo2;
        double nota_ganhador_rodada2;

        string ganhador_rodada1_jogo3;
        double nota_ganhador_rodada3;

        string ganhador_rodada1_jogo4;
        double nota_ganhador_rodada4;

        string ganhador_rodada2_jogo1;
        double nota_ganhador_rodada2_nota1;

        string ganhador_rodada2_jogo2;
        double nota_ganhador_rodada2_nota2;


        string ganhador_final;
        string segundo_colocado;
        int ganhador_final_jogo = 0;
        public async Task<ActionResult> Index()
        {
            string BaseUrlApi = "https://copadosfilmes.azurewebsites.net";
            ///api/filmes

            List<FilmeModel> Filmes = new List<FilmeModel>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseUrlApi);

                client.DefaultRequestHeaders.Clear();

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage Resposta = await client.GetAsync("/api/filmes");

                if (Resposta.IsSuccessStatusCode)
                {
                    var informacao = Resposta.Content.ReadAsStringAsync().Result;

                    Filmes = JsonConvert.DeserializeObject<List<FilmeModel>>(informacao);
                }

            }
            return View(Filmes);
        }

        [HttpPost]
        public JsonResult FilmesSelecionadosAjax(List<FilmesSelecionadosModel> filmes)
        {
          
            //Primeira rodada pegando os ganhadores de cada rodada , usando a funcao GanhadorRodada
            int informacao_ganhador1 = GanhadorRodada(filmes[0].filme.ToString(), filmes[0].nota, filmes[7].filme.ToString(), filmes[7].nota);
            int informacao_ganhador2 = GanhadorRodada(filmes[1].filme.ToString(), filmes[1].nota, filmes[6].filme.ToString(), filmes[6].nota);
            int informacao_ganhador3 = GanhadorRodada(filmes[2].filme.ToString(), filmes[2].nota, filmes[5].filme.ToString(), filmes[5].nota);
            int informacao_ganhador4 = GanhadorRodada(filmes[3].filme.ToString(), filmes[3].nota, filmes[4].filme.ToString(), filmes[4].nota);

            //Verificando os ganhadores
            if (informacao_ganhador1 == 1)
            {
                 ganhador_rodada1_jogo1 = filmes[0].filme.ToString();
                 nota_ganhador_rodada1 = filmes[0].nota;
            }
            else
            {

                 ganhador_rodada1_jogo1 = filmes[7].filme.ToString();
                 nota_ganhador_rodada1 = filmes[7].nota;
            }

            if (informacao_ganhador2 == 1)
            {
                 ganhador_rodada1_jogo2 = filmes[1].filme.ToString();
                 nota_ganhador_rodada2 = filmes[1].nota;
            }
            else
            {

                 ganhador_rodada1_jogo2 = filmes[6].filme.ToString();
                 nota_ganhador_rodada2 = filmes[6].nota;
            }


            if (informacao_ganhador3 == 1)
            {
                 ganhador_rodada1_jogo3 = filmes[2].filme.ToString();
                 nota_ganhador_rodada3 = filmes[2].nota;
            }
            else
            {

                 ganhador_rodada1_jogo3 = filmes[5].filme.ToString();
                 nota_ganhador_rodada3 = filmes[5].nota;
            }

            if (informacao_ganhador4 == 1)
            {
                 ganhador_rodada1_jogo4 = filmes[3].filme.ToString();
                 nota_ganhador_rodada4 = filmes[3].nota;
            }
            else
            {

                 ganhador_rodada1_jogo4 = filmes[4].filme.ToString();
                 nota_ganhador_rodada4 = filmes[4].nota;
            }


            //Segunda rodada pegando as informacaoes , usando a funcao GanhadorRodada
            informacao_ganhador_rodada2_1 = GanhadorRodada(ganhador_rodada1_jogo1, nota_ganhador_rodada1, ganhador_rodada1_jogo2, nota_ganhador_rodada2);
             informacao_ganhador_rodada2_2 = GanhadorRodada(ganhador_rodada1_jogo3, nota_ganhador_rodada3, ganhador_rodada1_jogo4, nota_ganhador_rodada4);

            //Verificando os ganhadores
            if (informacao_ganhador_rodada2_1 == 1)
            {
                ganhador_rodada2_jogo1 = ganhador_rodada1_jogo1;
                nota_ganhador_rodada2_nota1 = nota_ganhador_rodada1;
            }
            else
            {

                ganhador_rodada2_jogo1 = ganhador_rodada1_jogo2;
                nota_ganhador_rodada2_nota1 = nota_ganhador_rodada2;
            }


            if (informacao_ganhador_rodada2_2 == 1)
            {
                ganhador_rodada2_jogo2 = ganhador_rodada1_jogo3;
                nota_ganhador_rodada2_nota2 = nota_ganhador_rodada3;
            }
            else
            {
                ganhador_rodada2_jogo2 = ganhador_rodada1_jogo4;
                nota_ganhador_rodada2_nota2 = nota_ganhador_rodada4;
            }

            //Buscando o ganhador, , usando a funcao GanhadorRodada
            ganhador_final_jogo = GanhadorRodada(ganhador_rodada2_jogo1, nota_ganhador_rodada2_nota1, ganhador_rodada2_jogo2, nota_ganhador_rodada2_nota2);

            //Verificando o ganhador
            if(ganhador_final_jogo == 1)
            {
                ganhador_final = ganhador_rodada2_jogo1;
                segundo_colocado = ganhador_rodada2_jogo2;
            }
            else
            {

                ganhador_final = ganhador_rodada2_jogo2;
                segundo_colocado = ganhador_rodada2_jogo1;
            }

            //Retornando o json com o primeiro e segundo colocado para a funcao ajax que fez a chamada

            return Json(new { campeao = ganhador_final, segundo_colocado = segundo_colocado });

        }

        //Funcao de ganhador onde recebo os dois filmes e as duas notas de cada um
        private int GanhadorRodada(string filme1, double nota1, string filme2, double nota2)
        {
            //variavel usada para saber o ganhador
            int ganhador = 0;
            
            //Verifico se as notas são iguais, se forem eu faço a comparação dentro dela pelo maior em ordem alfabetica
            if (nota1 == nota2)
            {
                //Verifico se é maior por ordem alfabética enviando os dois nomes de filme
                int informacao = CompararMaiorPorOrdemAlfabetica(filme1, filme2);

                ///Verifico a informação e coloco a variavel ganhador seu respectivo ganhador
                if (informacao == 1)
                {
                    ganhador = 1;
                }
                else
                {
                    ganhador = 2;
                }

            }
            //Se a nota1 for maior que a nota 2 o primeiro filme que recebe a funcao ganha senão for o segundo ganha
            else if (nota1 > nota2)
            {
                ganhador =  1;
            }
            else
            {
                ganhador = 2;
            }

            return ganhador;
        }

        //Função para comparar as frases por ordem alfabetica, recebendo os dois nomes
        private int CompararMaiorPorOrdemAlfabetica(string filme1, string filme2)
        {
            int resultado = 0;

            //recebendo o valor da comparação
            int resultado_ganhador = filme1.CompareTo(filme2);

            //Se for um e que o filme um é maior e ganhou senao, o filme dois, que são passados para a função
            if(resultado_ganhador == 1)
            {
                resultado = 1;
            }
            else
            {
                resultado = 2;
            }

            return resultado;
        }

        //Função para abrir a view de campeao e segundo colocado
        public PartialViewResult VerificarCampeao(string primeiro, string segundo)
        {
            var filmeGanhadorModel = new GanhadorModel();

            filmeGanhadorModel.primeiro = primeiro;
            filmeGanhadorModel.segundo = segundo;

            return PartialView(filmeGanhadorModel);
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}