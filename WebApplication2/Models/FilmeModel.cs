﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class FilmeModel
    {
        public string id { get; set; }
        public string titulo { get; set; }
        public int ano { get; set; }
        public double nota { get; set; }
        

    }
    
    public class FilmesSelecionadosModel
    {

       public string filme { get; set; }
        public double nota { get; set; }


    }

    public class GanhadorModel
    {
        public string primeiro { get; set; }

        public string segundo { get; set; }
    }
    
}
